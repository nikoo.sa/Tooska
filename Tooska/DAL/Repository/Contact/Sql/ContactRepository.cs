﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppViewModel.ViewModel;
using DAL.Model;

namespace DAL.Repository.Contact.Sql
{
    public class ContactRepository:IContactRepository
    {
        private readonly TooskaEntities _context;

        public ContactRepository(TooskaEntities context)
        {
            _context = context;
        }

        public bool AddComment(ContactModel model)
        {
            var comment = new Model.Contact
            {
                Id = model.Id,
                Name = model.Name,
                Email = model.Email,
                Text = model.Text
                //image
            };
            _context.Contacts.Add(comment);
            _context.SaveChanges();
            return true;
        }
    }
}

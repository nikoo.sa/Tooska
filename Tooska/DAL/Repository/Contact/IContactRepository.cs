﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppViewModel.ViewModel;

namespace DAL.Repository.Contact
{
    public interface IContactRepository
    {
        bool AddComment(ContactModel model);
    }
}

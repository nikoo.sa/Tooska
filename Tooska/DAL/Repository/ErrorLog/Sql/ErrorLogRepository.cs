﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppViewModel.ViewModel;
using DAL.Model;

namespace DAL.Repository.ErrorLog.Sql
{
    public class ErrorLogRepository:IErrorLogRepository
    {
        private readonly TooskaEntities _context;

        public ErrorLogRepository(TooskaEntities context)
        {
            _context = context;
        }
        public bool AddErrorLog(ErrorLogModel model)
        {
            try
            {
                var errorLog = new Model.ErrorLog
                {
                    InsertTime = model.Moment,
                    Method = model.Method,
                    Input = model.Input,
                    ExceptionDescription = model.ExceptionDescription,
                    Message = model.Message
                };
                _context.ErrorLogs.Add(errorLog);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppViewModel.ViewModel;

namespace DAL.Repository.ErrorLog
{
    public interface IErrorLogRepository
    {
        bool AddErrorLog(ErrorLogModel model);
    }
}

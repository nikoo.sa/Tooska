﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppViewModel.ViewModel;
using AppViewModel.ViewModel.Enums;
using DAL.Model;

namespace DAL.Repository.Quote.Sql
{
    public class QuoteRepository:IQuoteRepository
    {
        private readonly TooskaEntities _context;

        public QuoteRepository(TooskaEntities context)
        {
            _context = context;
        }
        
        public List<QuoteModel> GetAllQuotes()
        {
            var result = new List<QuoteModel>();
            var quotes = _context.Quotes.Where(x => x.IsActive).OrderBy(x => x.Id).ToList();
            var model = new QuoteModel();
            foreach (var quote in quotes)
            {
                model.Id = quote.Id;
                model.Profession = quote.Profession;
                model.Quote = quote.QuoteBody;
                model.Quotee = quote.Quotee;
                result.Add(model);
            }
            return result;
        }

    }
}
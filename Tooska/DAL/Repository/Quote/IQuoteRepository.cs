﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppViewModel.ViewModel;

namespace DAL.Repository.Quote
{
    public interface IQuoteRepository
    {
        List<QuoteModel> GetAllQuotes();
    }
}

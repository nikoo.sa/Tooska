﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppViewModel.ViewModel;
using AppViewModel.ViewModel.Enums;
using DAL.Model;

namespace DAL.Repository.PresentableData.Sql
{
    public class PresentableDataRepository:IPresentableDataRepository
    {
        private readonly TooskaEntities _context;

        public PresentableDataRepository(TooskaEntities context)
        {
            _context = context;
        }
        public bool AddData(PresentableDataModel model)
        {
            var data = new Model.PresentableData
            {
                Id = model.Id,
                Description = model.Description,
                Title = model.Title,
                Type = (long)model.Type
                //image
            };
            _context.PresentableDatas.Add(data);
            _context.SaveChanges();
            return true;
        }

        public bool RemoveData(PresentableDataModel model)
        {
            var data = _context.PresentableDatas.FirstOrDefault(x => x.Id == model.Id);
            if (data != null) _context.PresentableDatas.Remove(data);
            _context.SaveChanges();
            return true;
        }

        public List<PresentableDataModel> GetKLastData(int k)
        {
            var result = new List<PresentableDataModel>();
            var datas = _context.PresentableDatas.Where(x => x.IsActive).OrderBy(x => x.Id).Skip(Math.Max(0, _context.PresentableDatas.Count() - k)).ToList();
            var model = new PresentableDataModel();
            foreach (var data in datas)
            {
                model.Id = data.Id;
                model.Description = data.Description;
                model.Title = data.Title;
                model.Type = (DataType) data.Type;
                result.Add(model);
            }
            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppViewModel.ViewModel;

namespace DAL.Repository.PresentableData
{
    public interface IPresentableDataRepository
    {
        bool AddData(PresentableDataModel model);
        bool RemoveData(PresentableDataModel model);
        List<PresentableDataModel> GetKLastData(int k);
    }
}

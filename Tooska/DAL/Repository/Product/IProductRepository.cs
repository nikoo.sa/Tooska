﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppViewModel.ViewModel;

namespace DAL.Repository.Product
{
    public interface IProductRepository
    {
        bool AddProduct(ProductModel model);
        bool RemoveProduct(ProductModel model);
        List<ProductModel> GetKLastProducts(int k);
        List<ProductModel> GetAllProducts();
        ProductModel GetProduct(long id);
    }
}

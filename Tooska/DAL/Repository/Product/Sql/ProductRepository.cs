﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppViewModel.ViewModel;
using DAL.Model;
using System.Configuration;

namespace DAL.Repository.Product.Sql
{
   public class ProductRepository : IProductRepository
    {
        private readonly TooskaEntities _context;

        public ProductRepository(TooskaEntities context)
        {
            _context = context;
        }
        public bool AddProduct(ProductModel model)
        {
            var product = new Model.Product
            {
                FullDescription = model.FullDescription,
                PreDescription = model.PreDescription,
                Id = model.Id,
                Link = model.Link,
                Name = model.Name,
                //image
            };
            _context.Products.Add(product);
            _context.SaveChanges();
            return true;
        }

        public bool RemoveProduct(ProductModel model)
        {
            var data = _context.Products.FirstOrDefault(x => x.Id == model.Id);
            if (data != null) _context.Products.Remove(data);
            _context.SaveChanges();
            return true;
        }

        public List<ProductModel> GetKLastProducts(int k)
        {
            var serverUrl = ConfigurationSettings.AppSettings["Server"] 
                + "/" + ConfigurationSettings.AppSettings["ShareImageUrl"];

            var result = new List<ProductModel>();
            var products = _context.Products.Where(x => x.IsActive).OrderBy(x => x.Id).Skip(Math.Max(0, _context.Products.Count() - k)).ToList();
            var model = new ProductModel();
            foreach (var product in products)
            {
                model.Id = product.Id;
                model.ThumPath = serverUrl + "/" + product.PathKey + "/" + product.ImageName;
                model.FullDescription = product.FullDescription;
                model.PreDescription = product.PreDescription;
                model.Link = product.Link;
                model.Name = product.Name;
                result.Add(model);
            }
            return result;
        }
        public List<ProductModel> GetAllProducts()
        {
            var result = new List<ProductModel>();
            var products = _context.Products.Where(x => x.IsActive).OrderBy(x => x.Id).ToList();
            var model = new ProductModel();
            foreach (var product in products)
            {
                model.Id = product.Id;
                model.FullDescription = product.FullDescription;
                model.PreDescription = product.PreDescription;
                model.Link = product.Link;
                model.Name = product.Name;
                result.Add(model);
            }
            return result;
        }
        public ProductModel GetProduct(long id)
        {
            var product = _context.Products.FirstOrDefault(x => x.IsActive && x.Id==id);

            var result = new ProductModel();
            result.Id = product.Id;
            result.FullDescription = product.FullDescription;
            result.PreDescription = product.PreDescription;
            result.Link = product.Link;
            result.Name = product.Name;

            return result;
        }
    }
 }

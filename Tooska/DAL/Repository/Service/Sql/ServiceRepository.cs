﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppViewModel.ViewModel;
using DAL.Model;

namespace DAL.Repository.Service.Sql
{
    public class ServiceRepository:IServiceRepository
    {
        private readonly TooskaEntities _context;

        public ServiceRepository(TooskaEntities context)
        {
            _context = context;
        }
        public bool AddService(ServiceModel model)
        {
            var service = new Model.Service
            {
                Id = model.Id,
                Description = model.Description,
                Title = model.Title
                //image
            };
            _context.Services.Add(service);
            _context.SaveChanges();
            return true;
        }

        public bool RemoveService(ServiceModel model)
        {
            var data = _context.Services.FirstOrDefault(x => x.Id == model.Id);
            if (data != null) _context.Services.Remove(data);
            _context.SaveChanges();
            return true;
        }

        public List<ServiceModel> GetKLastServices(int k)
        {
            var result = new List<ServiceModel>();
            var services = _context.Services.Where(x => x.IsActive).OrderBy(x => x.Id).Skip(Math.Max(0, _context.Services.Count() - k)).ToList();
            var model = new ServiceModel();
            foreach (var service in services)
            {
                model.Id = service.Id;
                model.Description = service.Description;
                model.Title = service.Title;
                result.Add(model);
            }
            return result;
        }
    }
}

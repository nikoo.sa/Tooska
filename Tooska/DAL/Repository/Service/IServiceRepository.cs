﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppViewModel.ViewModel;

namespace DAL.Repository.Service
{
    public interface IServiceRepository
    {
        bool AddService(ServiceModel model);
        bool RemoveService(ServiceModel model);
        List<ServiceModel> GetKLastServices(int k);
    }
}

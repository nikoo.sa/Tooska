﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppViewModel.ViewModel;
using DAL.Model;

namespace DAL.Repository.Customer.Sql
{
    public class CustomerRepository:ICustomerRepository
    {
        private readonly TooskaEntities _context;

        public CustomerRepository(TooskaEntities context)
        {
            _context = context;
        }
        public bool AddCustomer(CustomerModel model)
        {
            var customer = new Model.Customer
            {
                Id = model.Id,
                Name = model.Name
            };
            _context.Customers.Add(customer);
            _context.SaveChanges();
            return true;
        }

        public bool RemoveCustomer(CustomerModel model)
        {
            var data = _context.Customers.FirstOrDefault(x => x.Id == model.Id);
            if (data != null) _context.Customers.Remove(data);
            _context.SaveChanges();
            return true;
        }

        public List<CustomerModel> GetKLastCustomers(int k)
        {
            var result = new List<CustomerModel>();
            var customers = _context.Customers.Where(x => x.IsActive).OrderBy(x => x.Id).Skip(Math.Max(0, _context.Customers.Count() - k)).ToList();
            var model = new CustomerModel();
            foreach (var customer in customers)
            {
                model.Id = customer.Id;
                model.Name = customer.Name;
                result.Add(model);
            }
            return result;
        }

    }
}

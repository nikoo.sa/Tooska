﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppViewModel.ViewModel;

namespace DAL.Repository.Customer
{
    public interface ICustomerRepository
    {
        bool AddCustomer(CustomerModel model);
        bool RemoveCustomer(CustomerModel model);
        List<CustomerModel> GetKLastCustomers(int k);
    }
}

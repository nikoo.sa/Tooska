﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppViewModel.ViewModel;
using AppViewModel.ViewModel.Enums;
using DAL.Model;

namespace DAL.Repository.Footer.Sql
{
    public class FooterRepository:IFooterRepository
    {
        private readonly TooskaEntities _context;

        public FooterRepository(TooskaEntities context)
        {
            _context = context;
        }
        private DataType? GetEquivalentType(long type)///////unused:D
        {
            switch (type)
            {
                case 0:
                    return DataType.Header;
                case 1:
                    return DataType.AboutTooska;
                case 2:
                    return DataType.AboutSubscription;
                case 3:
                    return DataType.TooskaSubHeader;
                case 4:
                    return DataType.SubscriptionSubHeader;
                case 5:
                    return DataType.ServiceHeader;
                case 6:
                    return DataType.AboutFooter;
                case 7:
                    return DataType.ContactHeader;
                default:
                    return null;
            }
        }
        public PresentableDataModel GetFooterHeader()
        {
            var data = _context.PresentableDatas.FirstOrDefault(x => x.Type == (long) DataType.AboutFooter);
            if (data == null) return null;
            var result = new PresentableDataModel()
            {
                Type = DataType.AboutFooter,
                Description = data.Description,
                Id = data.Id,
                //Image = 
                Title = data.Title
            };
            return result;
        }
    }
}

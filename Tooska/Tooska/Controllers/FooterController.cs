﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Routing;
using AppViewModel.UIClasses;
using AppViewModel.ViewModel;
using DAL.Repository.ErrorLog;
using DAL.Repository.Footer;
using DAL.Repository.Product;

namespace Tooska.Controllers
{
    [RoutePrefix("api/Footer")]
    public class FooterController : ApiController
    {
        private readonly IProductRepository _productRepository;
        private readonly IFooterRepository _footerRepository;
        private readonly IErrorLogRepository _errorLogRepository;

        public FooterController(IProductRepository productRepository, IFooterRepository footerRepository, IErrorLogRepository errorLogRepository)
        {
            _productRepository = productRepository;
            _footerRepository = footerRepository;
            _errorLogRepository = errorLogRepository;
        }

       

        [AcceptVerbs("GET")]
        [Route("GetFooter")]
        public JsonResult<ResultUi> GetFooter()
        {
            ResultUi response;
            var temp = new FooterModel();
            try
            {
                temp.Products = _productRepository.GetKLastProducts(6);
                temp.Header = _footerRepository.GetFooterHeader();
                response = new ResultUi { Value = temp, Msg = "Success" };
            }
            catch (Exception ex)
            {
                response = new ResultUi { Value = null, Exception = ex.ToString(), Msg = "Fail" };
                _errorLogRepository.AddErrorLog(new ErrorLogModel("GetFooter_Data", "void", ex.Message, ex.ToString(), DateTime.Now));
            }
            return Json(response);
        }
    }
}

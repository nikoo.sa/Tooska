﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using AppViewModel.UIClasses;
using AppViewModel.ViewModel;
using DAL.Repository;
using DAL.Repository.Customer;
using DAL.Repository.ErrorLog;
using DAL.Repository.PresentableData;
using DAL.Repository.PresentableData.Sql;
using DAL.Repository.Product;
using DAL.Repository.Quote;
using DAL.Repository.Service;

namespace Tooska.Controllers
{
    [RoutePrefix("api/FirstPage")]
    public class FirstPageController : ApiController
    {
        private readonly IProductRepository _productRepository;
        private readonly IServiceRepository _serviceRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IPresentableDataRepository _presentableDataRepository;
        private readonly IErrorLogRepository _errorLogRepository;
        private readonly IQuoteRepository _quoteRepository;

        public FirstPageController(IProductRepository productRepository, IServiceRepository serviceRepository, ICustomerRepository customerRepository, IPresentableDataRepository presentableDataRepository, IErrorLogRepository errorLogRepository, IQuoteRepository quoteRepository)
        {
            _productRepository = productRepository;
            _serviceRepository = serviceRepository;
            _customerRepository = customerRepository;
            _presentableDataRepository = presentableDataRepository;
            _errorLogRepository = errorLogRepository;
            _quoteRepository = quoteRepository;
        }
        
        [AcceptVerbs("GET")]
        [Route("GetContent")]
        public JsonResult<ResultUi> GetFirstPageContent()
        {
            ResultUi response;
            var temp = new FirstPageModel();
            //   response.Value = new FirstPageModel();
            try
            {
                temp.Products = _productRepository.GetKLastProducts(4);
                temp.Customers = _customerRepository.GetKLastCustomers(5);
                temp.Services = _serviceRepository.GetKLastServices(3);
                temp.Datas = _presentableDataRepository.GetKLastData(3);
                temp.FirstPageQuote = _quoteRepository.GetAllQuotes();

                response = new ResultUi {Value = temp, Msg = "Success"};
            }
            catch (Exception ex)
            {
                response = new ResultUi { Value = null, Exception = ex.ToString(), Msg = "Fail"};
                _errorLogRepository.AddErrorLog(new ErrorLogModel("GetFirstPageContent_Data", "void", ex.Message, ex.ToString(), DateTime.Now));
            }
            return Json(response);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using AppViewModel.UIClasses;
using AppViewModel.ViewModel;
using DAL.Repository.Contact;
using DAL.Repository.ErrorLog;

namespace Tooska.Controllers
{
    [RoutePrefix("api/Contact")]
    public class ContactController : ApiController
    {
        private readonly IContactRepository _contactRepository;
        private readonly IErrorLogRepository _errorLogRepository;

        public ContactController(IContactRepository contactRepository, IErrorLogRepository errorLogRepository)
        {
            _contactRepository = contactRepository;
            _errorLogRepository = errorLogRepository;
        }

        [AcceptVerbs("POST")]
        [Route("ContactUs")]
        public JsonResult<ResultUi> ContactUs(ContactModel model)
        {
            ResultUi response;
            try
            {
                _contactRepository.AddComment(model);
                response = new ResultUi {Value = "comment added successfully", Msg = "Success"};
            }
            catch (Exception ex)
            {
                response = new ResultUi {Value = null, Exception = ex.ToString(), Msg = "Fail" };
                _errorLogRepository.AddErrorLog(new ErrorLogModel("ContactUs", "void", ex.Message, ex.ToString(), DateTime.Now));
            }
            return Json(response);
        }
    }
}

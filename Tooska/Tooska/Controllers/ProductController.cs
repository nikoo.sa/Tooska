﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using AppViewModel.UIClasses;
using AppViewModel.ViewModel;
using DAL.Repository.ErrorLog;
using DAL.Repository.Product;

namespace Tooska.Controllers
{
    [RoutePrefix("api/Product")]
    public class ProductController : ApiController
    {
        private readonly IProductRepository _productRepository;
        private readonly IErrorLogRepository _errorLogRepository;
        public ProductController(IProductRepository productRepository, IErrorLogRepository errorLogRepository)
        {
            _productRepository = productRepository;
            _errorLogRepository = errorLogRepository;
        }
        
        [AcceptVerbs("Get")]
        [Route("GetProducts")]
        public JsonResult<ResultUi> GetProducts()
        {
            ResultUi response;
            try
            {
                response = new ResultUi {Value = _productRepository.GetAllProducts(), Msg = "Success"};
            }
            catch (Exception ex)
            {
                response = new ResultUi { Value = null, Exception = ex.ToString(), Msg = "Fail" };
                _errorLogRepository.AddErrorLog(new ErrorLogModel("GetAllProducts", "void", ex.Message, ex.ToString(), DateTime.Now));
            }
            return Json(response);
        }
        [AcceptVerbs("Get")]
        [Route("GetProducts")]
        public JsonResult<ResultUi> GetSpecifiedProduct(long id)
        {
            ResultUi response;
            try
            {
                response = new ResultUi { Value = _productRepository.GetProduct(id), Msg = "Success" };
            }
            catch (Exception ex)
            {
                response = new ResultUi { Value = null, Exception = ex.ToString(), Msg = "Fail" };
                _errorLogRepository.AddErrorLog(new ErrorLogModel("GetSpecifiedProduct", "void", ex.Message, ex.ToString(), DateTime.Now));
            }
            return Json(response);
        }
    }
}

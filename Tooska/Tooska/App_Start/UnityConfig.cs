using Microsoft.Practices.Unity;
using System.Web.Http;
using DAL.Model;
using DAL.Repository.Contact;
using DAL.Repository.Contact.Sql;
using DAL.Repository.Customer;
using DAL.Repository.Customer.Sql;
using DAL.Repository.ErrorLog.Sql;
using Unity.WebApi;
using DAL.Repository.ErrorLog;
using DAL.Repository.Footer;
using DAL.Repository.Footer.Sql;
using DAL.Repository.PresentableData.Sql;
using DAL.Repository.PresentableData;
using DAL.Repository.Service;
using DAL.Repository.Service.Sql;
using DAL.Repository.Product.Sql;
using DAL.Repository.Product;
using DAL.Repository.Quote;
using DAL.Repository.Quote.Sql;

namespace Tooska
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<ICustomerRepository, CustomerRepository>();
            container.RegisterType<IErrorLogRepository, ErrorLogRepository>();
            container.RegisterType<IPresentableDataRepository, PresentableDataRepository>();
            container.RegisterType<IProductRepository, ProductRepository>();
            container.RegisterType<IServiceRepository, ServiceRepository>();
            container.RegisterType<IFooterRepository, FooterRepository>();
            container.RegisterType<IContactRepository, ContactRepository>();
            container.RegisterType<IQuoteRepository, QuoteRepository>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}
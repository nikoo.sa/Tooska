﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using DAL.Repository.Contact;
using DAL.Repository.Contact.Sql;
using DAL.Repository.Customer;
using DAL.Repository.Customer.Sql;
using DAL.Repository.ErrorLog;
using DAL.Repository.ErrorLog.Sql;
using DAL.Repository.Footer;
using DAL.Repository.Footer.Sql;
using DAL.Repository.PresentableData;
using DAL.Repository.PresentableData.Sql;
using DAL.Repository.Product;
using DAL.Repository.Product.Sql;
using DAL.Repository.Quote;
using DAL.Repository.Quote.Sql;
using DAL.Repository.Service;
using DAL.Repository.Service.Sql;
using Microsoft.Practices.Unity;
using Unity.WebApi;

namespace Tooska
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            var container = new UnityContainer();

            container.RegisterType<ICustomerRepository, CustomerRepository>
                (new HierarchicalLifetimeManager());
            container.RegisterType<IErrorLogRepository, ErrorLogRepository>
                (new HierarchicalLifetimeManager());
            container.RegisterType<IPresentableDataRepository, PresentableDataRepository>
                (new HierarchicalLifetimeManager());
            container.RegisterType<IProductRepository, ProductRepository>
                (new HierarchicalLifetimeManager());
            container.RegisterType<IServiceRepository, ServiceRepository>
                (new HierarchicalLifetimeManager());
            container.RegisterType<IFooterRepository, FooterRepository>
                (new HierarchicalLifetimeManager());
            container.RegisterType<IContactRepository, ContactRepository>
                (new HierarchicalLifetimeManager());
            container.RegisterType<IQuoteRepository, QuoteRepository>
                (new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityDependencyResolver(container);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}

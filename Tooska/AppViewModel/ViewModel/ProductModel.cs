﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppViewModel.ViewModel
{
    public class ProductModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string FullDescription { get; set; }
        public string PreDescription { get; set; }
        public string Link { get; set; }
        public string ThumPath { get; set; }
        // public string Image { get; set; }
    }
}

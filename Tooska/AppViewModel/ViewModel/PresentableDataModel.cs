﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppViewModel.ViewModel.Enums;

namespace AppViewModel.ViewModel
{
    public class PresentableDataModel
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DataType Type { get; set; }
       // public string Image { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppViewModel.ViewModel
{
    public class FirstPageModel
    {
        public List<ProductModel> Products { get; set; }
        public List<CustomerModel> Customers { get; set; }
        public List<PresentableDataModel> Datas { get; set; }
        public List<ServiceModel> Services { get; set; }
        public List<QuoteModel> FirstPageQuote { get; set; }
    }
}

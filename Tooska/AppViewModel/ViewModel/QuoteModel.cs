﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppViewModel.ViewModel
{
    public class QuoteModel
    {
        public long Id { get; set; }
        public string Quote { get; set; }
        public string Quotee { get; set; }
        public string Profession { get; set; }
    }
}

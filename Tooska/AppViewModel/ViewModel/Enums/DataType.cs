﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppViewModel.ViewModel.Enums
{
    public enum DataType
    {
        Header = 0,
        AboutTooska = 1,
        AboutSubscription = 2,
        TooskaSubHeader = 3,
        SubscriptionSubHeader = 4,
        ServiceHeader = 5,
        AboutFooter = 6,
        ContactHeader = 7
    }
}

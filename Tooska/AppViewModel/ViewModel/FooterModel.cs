﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppViewModel.ViewModel.Enums;

namespace AppViewModel.ViewModel
{
    public class FooterModel
    {
        public List<ProductModel> Products { get; set; }
        public PresentableDataModel Header { get; set; }
    }
}

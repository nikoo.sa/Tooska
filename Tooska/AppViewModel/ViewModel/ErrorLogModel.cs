﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppViewModel.ViewModel
{
    public class ErrorLogModel
    {
        public DateTime Moment { get; set; }
        public string ExceptionDescription { get; set; }
        public string Method { get; set; }
        public string Input { get; set; }
        public string Message { get; set; }

        public ErrorLogModel(string methodName, string input, string message, string description, DateTime moment)
        {
            Method = methodName;
            Input = input;
            Message = message;
            ExceptionDescription = description;
            Moment = moment;
        }
    }
}

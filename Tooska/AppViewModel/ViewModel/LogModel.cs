﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppViewModel.ViewModel
{
    public class LogModel
    {
        public DateTime Moment { get; set; }
        public string Result { get; set; }
        public string Method { get; set; }
        public string Input { get; set; }
        public string Output { get; set; }

        public LogModel(string methodName, string input, string output, string result, DateTime moment)
        {
            Method = methodName;
            Input = input;
            Output = output;
            Result = result;
            Moment = moment;
        }
    }
}
